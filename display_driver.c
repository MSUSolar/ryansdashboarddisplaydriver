/*
 * display_driver.c
 *
 *  Created on: Mar 29, 2016
 *      Author: Ryan
 */

# include <msp430.h>
int shifter(unsigned char reg_val,  int select, int place, unsigned char temp){

	temp >>= select;
	temp &= 1;
	temp <<= place;
	reg_val = (reg_val & ~(1 << place)) | temp;
	return reg_val;

}

void writeDisplays ( unsigned char cal1, unsigned char cal2, unsigned char cal3, unsigned char cal4, int row){





		LCDM3 = shifter(LCDM3, 0, row, cal1);
		LCDM4 = shifter(LCDM4, 1, row, cal1);
		LCDM5 = shifter(LCDM5, 2, row, cal1);
		LCDM6 = shifter(LCDM6, 3, row, cal1);
		LCDM7 = shifter(LCDM7, 4, row, cal1);
		LCDM8 = shifter(LCDM8, 5, row, cal1);
		LCDM9 = shifter(LCDM9, 6, row, cal1);
		LCDM10 = shifter(LCDM10, 7, row, cal1);






		LCDM11 = shifter(LCDM11, 0, row, cal2);
		LCDM12 = shifter(LCDM12, 1, row, cal2);
		LCDM13 = shifter(LCDM13, 2, row, cal2);
		LCDM14 = shifter(LCDM14, 3, row, cal2);
		LCDM15 = shifter(LCDM15, 4, row, cal2);
		LCDM16 = shifter(LCDM16, 5, row, cal2);
		LCDM17 = shifter(LCDM17, 6, row, cal2);
		LCDM18 = shifter(LCDM18, 7, row, cal2);




		LCDM19 = shifter(LCDM19, 0, row, cal3);
		LCDM20 = shifter(LCDM20, 1, row, cal3);
		LCDM21 = shifter(LCDM21, 2, row, cal3);
		LCDM22 = shifter(LCDM22, 3, row, cal3);
		LCDM23 = shifter(LCDM23, 4, row, cal3);
		LCDM24 = shifter(LCDM24, 5, row, cal3);
		LCDM25 = shifter(LCDM25, 6, row, cal3);
		LCDM26 = shifter(LCDM26, 7, row, cal3);




		LCDM27 = shifter(LCDM27, 0, row, cal4);
		LCDM28 = shifter(LCDM28, 1, row, cal4);
		LCDM29 = shifter(LCDM29, 2, row, cal4);
		LCDM30 = shifter(LCDM30, 3, row, cal4);
		LCDM31 = shifter(LCDM31, 4, row, cal4);




}

