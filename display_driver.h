/*
 * display_driver.h
 *
 *  Created on: Mar 29, 2016
 *      Author: Ryan
 */

#ifndef DISPLAY_DRIVER_H_
#define DISPLAY_DRIVER_H_

void writeDisplays ( unsigned char cal1, unsigned char cal2, unsigned char cal3, unsigned char cal4, int row);

#endif /* DISPLAY_DRIVER_H_ */
